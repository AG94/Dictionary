var fs = require('fs');

module.exports = {
    add: function(key, value){
        var store = this.readFile();
        store = store ? store : {};
        store[key] = value;
        this.writeFile(store);
        return "Added successfully!";
    },
    list: function(){
        var store = this.readFile();
        for (var key in store) {
            if (store.hasOwnProperty(key)) {
                console.log(key + ": " + store[key]);
            }
        }
        return Object.keys(store).length === 0 ? "List is empty!" : "That is all!";
    },
    get: function(key){
        var store = this.readFile();
        return store[key] ? store[key] : "Wrong key !!";
    },
    remove: function(key){
        var store = this.readFile();
        delete store[key];
        this.writeFile(store);
        return "Removed!";
    },
    clear: function(){
        this.writeFile({});
        return "Cleared all data successfully!";
    },
    readFile: function(){
        return require("./data.json");
    },
    writeFile: function(data){
        fs.writeFile( "data.json", JSON.stringify(data), "utf8", function(){ });
    }
}
require('make-runnable/custom')({
    printOutputFrame: false
});

